<?php
/**
 * @package shippingMethod
 * @copyright Copyright 2007-2015 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2015 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: fedexwebservicesfreight.php 4 2015-03-10 22:37:08Z numinix $
 */

class fedexwebservicesfreight {

	var $code;
	var $title;
	var $description;
	var $icon;
	var $enabled;
	var $account;
	var $password;
	var $zip;

	function fedexwebservicesfreight() {
		global $order, $db;

		// numinix reg key
		/*@define('MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_KEY', '6W02EnQC0n9nO5NH');
		@define('MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_PWD', 'tmZlwVIuLUHGNtasKOHQYkKKd');*/

		$freight_regkey = explode('|', MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_REGKEY);
		@define('MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_KEY', $freight_regkey[0]);
		@define('MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_PWD', $freight_regkey[1]);

		$this->code = 'fedexwebservicesfreight';
		$this->title = MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_TEXT_TITLE;
		if (extension_loaded('soap')) {
			$this->description      = MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_TEXT_DESCRIPTION;
		} else {
			$this->description      = MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_TEXT_DESCRIPTION_SOAP;
		}
		$this->icon = '';
		$this->sort_order = MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SORT_ORDER;
		$this->error = false;
		$this->fedex_key        = MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_KEY;
		$this->fedex_pwd        = MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_PWD;
		$this->fedex_act_num    = MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ACT_NUM;
		$this->fedex_meter_num  = MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_METER_NUM;

		if (zen_get_shipping_enabled($this->code)) {
			if (extension_loaded('soap')) {
				$this->enabled = ((MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_STATUS == 'true') ? true : false);
			}
		}
		if ($this->enabled && (int)MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ZONE > 0) {
			$check_flag = false;
			$check = $db->Execute("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ZONE . "' and zone_country_id = '" . $order->delivery['country']['id'] . "' order by zone_id");
			while (!$check->EOF) {
				if ($check->fields['zone_id'] < 1) {
					$check_flag = true;
					break;
				} elseif ($check->fields['zone_id'] == $order->delivery['zone_id']) {
					$check_flag = true;
					break;
				}
				$check->MoveNext();
			}

			if ($check_flag == false) {
				$this->enabled = false;
			}
		}
	}

	function quote($method = '') {

		global $order, $shipping_weight, $shipping_num_boxes, $db;
		require_once(DIR_WS_INCLUDES . 'library/fedex-common.php5');
		$path_to_wsdl = DIR_WS_MODULES . 'shipping/fedexwebservicesfreight/wsdl/RateService_v16.wsdl';
		ini_set("soap.wsdl_cache_enabled", "0");
		$client = new SoapClient($path_to_wsdl, array('trace' => 1, 'connection_timeout' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_CONNECTION_TIMEOUT));

		if ($this->enabled && isset($order->delivery['postcode'])) {
			$service_types = array();
			if (MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_PRIORITY == 'true') {
				$service_types['FEDEX_FREIGHT_PRIORITY'] = array('icon' => '');
			}
			if (MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ECONOMY == 'true') {
				$service_types['FEDEX_FREIGHT_ECONOMY'] = array('icon' => '');
			}
			// build requests
			$request = $this->build_request($client, true);
			$this->quotes = $this->do_request($method, $request, $client, $service_types);

			return $this->quotes;
		}
	}

	function build_request($client, $allow_0_weight_shipping = true) {
		global $order, $shipping_weight, $shipping_num_boxes, $db;
		$request['WebAuthenticationDetail'] = array('UserCredential' =>
																					array('Key' => $this->fedex_key, 'Password' => $this->fedex_pwd));
		$request['ClientDetail'] = array('AccountNumber' => $this->fedex_act_num, 'MeterNumber' => $this->fedex_meter_num);
		$request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Request v16 using PHP ***');
		$request['Version'] = array('ServiceId' => 'crs', 'Major' => '16', 'Intermediate' => '0', 'Minor' => '0');
		$request['ReturnTransitAndCommit'] = 'true';
		$request['RequestedShipment']['Shipper'] = array('Address' => array(
			//'StreetLines' => array(MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ADDRESS_1, MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ADDRESS_2),
			'City' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_CITY,
			'StateOrProvinceCode' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_STATE,
			'PostalCode' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_ZIP,
			'CountryCode' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_CTRY));

		// customer details
		$street_address = $order->delivery['street_address'];
		$street_address2 = $order->delivery['suburb'];
		$city = $order->delivery['city'];
		$state = zen_get_zone_code($order->delivery['country']['id'], $order->delivery['zone_id'], '');
		if ($state == "QC") $state = "PQ";
		$postcode = str_replace(array(' ', '-'), '', $order->delivery['postcode']);
		$country_id = $order->delivery['country']['iso_code_2'];

		$residential_address = true;
		if (MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ADDRESS_VALIDATION == 'true' && isset($_SESSION['customer_id'])) {
			$path_to_address_validation_wsdl = DIR_WS_MODULES . 'shipping/fedexwebservicesfreight/wsdl/AddressValidationService_v2.wsdl';
			$av_client = new SoapClient($path_to_address_validation_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
			$av_request['WebAuthenticationDetail'] = array('UserCredential' =>
			array('Key' => $this->fedex_key, 'Password' => $this->fedex_pwd));
			$av_request['ClientDetail'] = array('AccountNumber' => $this->fedex_act_num, 'MeterNumber' => $this->fedex_meter_num);
			$av_request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Address Validation Request v2 using PHP ***');
			$av_request['Version'] = array('ServiceId' => 'aval', 'Major' => '2', 'Intermediate' => '0', 'Minor' => '0');
			$av_request['RequestTimestamp'] = date('c');
			$av_request['Options'] = array('CheckResidentialStatus' => 1,
			 'VerifyAddress' => 1,
			 'MaximumNumberOfMatches' => 10,
			 'StreetAccuracy' => 'MEDIUM',
			 'DirectionalAccuracy' => 'MEDIUM',
			 'CompanyNameAccuracy' => 'MEDIUM',
			 'ConvertToUpperCase' => 1,
			 'RecognizeAlternateCityNames' => 1,
			 'ReturnParsedElements' => 1);
			$av_request['AddressesToValidate'] = array(
				0 => array(
					'AddressId' => 'Customer Address',
					'Address' => array(
						'StreetLines' => array(utf8_encode($street_address), utf8_encode($street_address2)),
						'PostalCode' => $postcode,
						'City' => $city,
						'StateOrProvinceCode' => $state,
						'CompanyName' => $order->delivery['company'],
						'CountryCode' => $country_id
					)
				)
			);
			try {
				$av_response = $av_client->addressValidation($av_request);
				/*
				//echo '<!--';
				echo '<pre>';
				print_r($av_response);
				echo '</pre>';
				//echo '-->';
				die();
				*/
				if ($av_response->HighestSeverity == 'SUCCESS') {
					if ($av_response->AddressResults->ProposedAddressDetails->ResidentialStatus == 'BUSINESS') {
						$residential_address = false;
					} // already set to true so no need for else statement
				}
			} catch (Exception $e) {
			}
		}

		$request['RequestedShipment']['Recipient'] = array('Address' => array (
			'StreetLines' => array(utf8_encode($street_address), utf8_encode($street_address2)), // customer street address
			'City' => utf8_encode($city), //customer city
			//'StateOrProvinceCode' => $state, //customer state
			'PostalCode' => $postcode, //customer postcode
			'CountryCode' => $country_id, //customer county code
			'Residential' => $residential_address
			));
		if (in_array($country_id, array('US', 'CA'))) {
			$request['RequestedShipment']['Recipient']['Address']['StateOrProvinceCode'] = $state;
		}
		$request['RequestedShipment']['ShippingChargesPayment'] = array(
			'PaymentType' => 'SENDER',
			'Payor' => array('ResponsibleParty' => array('AccountNumber' => (MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO == 'true') ? MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_3RD_PARTY_ACT_NUM : MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIPPER_ACT_NUM)));
		if( MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO == 'true' ){
			$request['RequestedShipment']['FreightShipmentDetail'] = array(
				'AlternateBilling' => array(
					'AccountNumber' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_3RD_PARTY_ACT_NUM,
					'Address' => array(
						'StreetLines' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_ADDRS,
						'City' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_CITY,
						'StateOrProvinceCode' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_STATE,
						'PostalCode' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_ZIP,
						'CountryCode' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_CTRY
						)
					),
				'Role' => 'SHIPPER'
				);
		} else {
			$request['RequestedShipment']['FreightShipmentDetail'] = array(
				'FedExFreightAccountNumber' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIPPER_ACT_NUM,
				'FedExFreightBillingContactAndAddress' => array(
					'Contact'=>array(
						'ContactId' => 'freight1',
						'PersonName' => STORE_OWNER,
						'Title' => 'Owner',
						'CompanyName' => STORE_NAME,
						'PhoneNumber' => STORE_TELEPHONE_CUSTSERVICE
						),
					'Address'=>array(
						'StreetLines'=> MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_ADDRS,
						'City' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_CITY,
						'StateOrProvinceCode' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_STATE,
						'PostalCode' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_ZIP,
						'CountryCode' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_CTRY
						)
					),
				'Role' => 'SHIPPER'
				);
		}

		$request['RequestedShipment']['FreightShipmentDetail']['LineItems'] = array();

		// bof integration with Dimensions (Numinix Product Fields)
		// check for ready to ship field
		if (MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_READY_TO_SHIP == 'true') {
			$packages = array('default' => 0);
			$new_shipping_num_boxes = 0;
			foreach ($_SESSION['cart']->get_products() as $product) {
				$dimensions_query = "SELECT products_length, products_width, products_height, products_ready_to_ship_freight, products_dim_type FROM " . TABLE_PRODUCTS . "
					WHERE products_id = " . (int)$product['id'] . "
					AND products_length > 0
					AND products_width > 0
					AND products_height > 0
					LIMIT 1;";
				$dimensions = $db->Execute($dimensions_query);
				if ($dimensions->RecordCount() > 0 && $dimensions->fields['products_ready_to_ship_freight'] == 1) {
					for ($i = 1; $i <= $product['quantity']; $i++) {
						$packages[] = array('weight' => $product['weight'], 'length' => $dimensions->fields['products_length'], 'width' => $dimensions->fields['products_width'], 'height' => $dimensions->fields['products_height'], 'units' => strtoupper($dimensions->fields['products_dim_type']));
					}
				} else {
					$packages['default'] += $product['weight'] * $product['quantity'];
				}
			}
			if (count($packages) > 1) {
				$za_tare_array = preg_split("/[:,]/" , SHIPPING_BOX_WEIGHT);
				$zc_tare_percent= $za_tare_array[0];
				$zc_tare_weight= $za_tare_array[1];

				$za_large_array = preg_split("/[:,]/" , SHIPPING_BOX_PADDING);
				$zc_large_percent= $za_large_array[0];
				$zc_large_weight= $za_large_array[1];
			}
			foreach ($packages as $id => $values) {
				if ($id === 'default') {
					// divide the weight by the max amount to be shipped (can be done inside loop as this occurance should only ever happen once
					// note $values is not an array
					if ($values == 0) continue;
					$fedex_shipping_num_boxes = ceil((float)$values / (float)MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_MAX_WEIGHT);
					if ($fedex_shipping_num_boxes < 1) $fedex_shipping_num_boxes = 1;
					$fedex_shipping_weight = round((float)$values / $fedex_shipping_num_boxes, 2); // 2 decimal places max
					$boxed_value = sprintf("%01.2f", $this->insurance / $fedex_shipping_num_boxes);
					for ($i=0; $i<$fedex_shipping_num_boxes; $i++) {
						$new_shipping_num_boxes++;
						if (MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_MAX_WEIGHT <= $fedex_shipping_weight) {
							$fedex_shipping_weight = $fedex_shipping_weight + ($fedex_shipping_weight*($zc_large_percent/100)) + $zc_large_weight;
						} else {
							$fedex_shipping_weight = $fedex_shipping_weight + ($fedex_shipping_weight*($zc_tare_percent/100)) + $zc_tare_weight;
						}
						if ($fedex_shipping_weight <= 0) $fedex_shipping_weight = 0.1;
						$new_shipping_weight += $fedex_shipping_weight;
						$request['RequestedShipment']['FreightShipmentDetail']['LineItems'][] = array(
							'FreightClass' => 'CLASS_' . MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_FREIGHTCLASS,
							'Packaging' => 'PALLET',
							'Weight' => array(
								'Value' => $fedex_shipping_weight,
								'Units' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_WEIGHT
							)
						);
					}
				} else {
					$boxed_value = sprintf("%01.2f", $this->insurance / count($packages));
					// note $values is an array
					$new_shipping_num_boxes++;
					if ($values['weight'] <= 0) $values['weight'] = 0.1;
					$new_shipping_weight += $values['weight'];
					$request['RequestedShipment']['FreightShipmentDetail']['LineItems'][] = array(
						'FreightClass' => 'CLASS_' . MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_FREIGHTCLASS,
						'Packaging' => 'PALLET',
						'Weight' => array(
							'Value' => $values['weight'],
							'Units' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_WEIGHT
						),
						'Dimensions' => array(
							'Length' => ($values['length'] >= 1 ? $values['length'] : 1),
							'Width' => ($values['width'] >= 1 ? $values['width'] : 1),
							'Height' => ($values['height'] >= 1 ? $values['height'] : 1),
							'Units' => $values['units']
						)
					);
				}
			}
			$shipping_num_boxes = $new_shipping_num_boxes;
			$shipping_weight = round($new_shipping_weight / $shipping_num_boxes, 2);
		// eof integration with Dimensions (Numinix Product Fields)
		} else {
		// normal shipping with no dimensions
			for( $i=1; $i<=$shipping_num_boxes; $i++ ){
				$request['RequestedShipment']['FreightShipmentDetail']['LineItems'][] = array(
					'FreightClass' => 'CLASS_' . MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_FREIGHTCLASS,
					'Packaging' => 'PALLET',
					'Weight' => array(
							'Value' => ceil($shipping_weight),
							'Units' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_WEIGHT)
					);
			}
		}
		$request['RequestedShipment']['RateRequestTypes'] = 'NONE';
		// DEBUG: output array to screen
		/*
		echo '<pre>';
		echo print_r($request);
		echo '</pre>';
		*/
		return $request;

	}

	function do_request($method = '', $request, $client, $service_types) {
		global $db, $shipping_weight, $shipping_num_boxes, $cart, $order, $all_products_ship_free;
		try {
			$response = $client->getRates($request);
			// DEBUG: output array to screen
/*			echo '<pre>';
			echo print_r($client->__getLastRequest());
			echo '</pre>';
			echo '<pre>';
			print_r($response);
			echo '</pre>';//die()*/;

			if( MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_DEBUG == 'true' ){
				$log_time_stamp = microtime();
				error_log('['. strftime("%Y-%m-%d %H:%M:%S") .'] '. var_export($request, true), 3, DIR_FS_LOGS . '/fedexwebservicesfreight-requests-' . $log_time_stamp . '.log');
				error_log('['. strftime("%Y-%m-%d %H:%M:%S") .'] '. var_export($response, true), 3, DIR_FS_LOGS . '/fedexwebservicesfreight-responses-' . $log_time_stamp . '.log');
			}

			if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR' && is_array($response->RateReplyDetails) || is_object($response->RateReplyDetails)) {
				if (is_object($response->RateReplyDetails)) {
					$response->RateReplyDetails = get_object_vars($response->RateReplyDetails);
				}
				//echo '<pre>';
			 // print_r($response->RateReplyDetails);
				//echo '</pre>';
				switch (SHIPPING_BOX_WEIGHT_DISPLAY) {
					case (0):
					$show_box_weight = '';
					break;
					case (1):
					$show_box_weight = ' (' . $shipping_num_boxes . ' ' . MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIPPING_UNIT_NAME . ')';
					break;
					case (2):
					$show_box_weight = ' (' . number_format($shipping_weight * $shipping_num_boxes,2) . TEXT_SHIPPING_WEIGHT . ')';
					break;
					case (3):
					$show_box_weight = ' (' . $fedex_shipping_num_boxes . ' ' . MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIPPING_UNIT_NAME .' x ' . number_format($fedex_shipping_weight/$fedex_shipping_num_boxes,2) . TEXT_SHIPPING_WEIGHT . ')';
					default:
					$show_box_weight = ' (' . $shipping_num_boxes . ' x ' . number_format($shipping_weight,2) . TEXT_SHIPPING_WEIGHT . ')';
					break;
				}
				$quotes = array('id' => $this->code,
					'module' => $this->title . $show_box_weight,
					'info' => MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_INFO
					);
				$methods = array();
				foreach ($response->RateReplyDetails as $rateReply) {
					if (array_key_exists($rateReply->ServiceType, $service_types) && ($method == '' || str_replace('_', '', $rateReply->ServiceType) == $method)) {
						foreach($rateReply->RatedShipmentDetails as $ShipmentRateDetail) {
								$cost = $ShipmentRateDetail->TotalNetCharge->Amount;
								$cost = (float)round(preg_replace('/[^0-9.]/', '',  $cost), 2) + MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SURCHARGE + ($cost * (MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SURCHARGE_PERCENT / 100));
						}

						if ($cost > 0) {
							$methods[] = array('id' => str_replace('_', '', $rateReply->ServiceType),
								'title' => ucwords(strtolower(str_replace('_', ' ', $rateReply->ServiceType))),
								'cost' => $cost);
						}
					}
				}
				if (sizeof($methods) == 0) return false;
				$quotes['methods'] = $methods;
				if ($this->tax_class > 0) {
					$quotes['tax'] = zen_get_tax_rate($this->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
				}
			} else {
			    $message = 'Error in processing transaction.<br /><br />';
			    $message .= (is_array($response) ? $response['response']->Notifications->Severity : $response->Notifications->Severity );
			    $message .= ': ';
			    $message .= (is_array($response) ? $response['response']->Notifications->Message : $response->Notifications->Message ) . '<br />';
			    $quotes = array('module' => $this->title,
			        'error'  => $message);
			}
			if (zen_not_null($this->icon)) $this->quotes['icon'] = zen_image($this->icon, $this->title);
		} catch (SoapFault $exception) {
			$quotes = array('module' => $this->title,
				'error'  => 'Sorry, the FedEx.com server is currently not responding, please try again later.'.printFault($exception, $client));
		}
		//echo '<!-- Quotes: ';
		//print_r($this->quotes);
		//print_r($_SESSION['shipping']);
		//echo ' -->';
		return $quotes;
	}

	function check() {
		global $db;
		if (!isset($this->_check)) {
			$check_query = $db->Execute("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_STATUS'");
			$this->_check = $check_query->RecordCount();
			if ($this->_check && defined('MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_VERSION')) {
				switch(MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_VERSION) {
					case '1.0.1':
						$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Ready to Ship', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_READY_TO_SHIP', 'false', 'Enable using products_ready_to_ship_freight field (requires Numinix Product Fields optional dimensions fields) to identify products which ship separately?', '6', '10', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
						$db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '1.1.0' WHERE configuration_key = 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_VERSION' LIMIT 1;");
					case '1.1.0':
						break;
				}
			} elseif ($this->_check) {
				$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Version Installed', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_VERSION', '1.0.1', '', '6', '0', now())");
			}
		}
		return $this->_check;
	}

	function install() {
		global $db;

		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable FedEx LTL Shipping', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_STATUS', 'true', 'Do you want to offer Fedex Freight LTL shipping?', '6', '1', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Version Installed', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_VERSION', '1.1.0', '', '6', '0', 'zen_cfg_select_option(array(\'1.1.0\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Your FedEx Account Number', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ACT_NUM', '', 'Enter the FedEx Account Number assigned to you', '6', '2', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('FedEx Meter Number', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_METER_NUM', '', 'Enter FedEx Meter Number (You can get one at <a href=\"http://www.fedex.com/us/developer/\" target=\"_blank\">http://www.fedex.com/us/developer/</a>)', '6', '4', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Your FedEx Reg Key', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_REGKEY', '', 'Enter the FedEx Registration Key and Password assigned to you (separated by \'|\'). This is a number just for FEDEX, the password will be sent in the email after you register for a key (required).<br/><b>Example:</b> 1234567890|0123456789', '6', '2', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Freight LTL Shipper Number', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIPPER_ACT_NUM', '', 'Enter Freight LTL Shipper Account Number:', '6', '4', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Connection Timeout', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_CONNECTION_TIMEOUT', '15', 'Enter the maximum time limit in seconds that the server should wait when connecting to the FedEx server.', '6', '10', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Guarantee', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_GUARANTEE', 'false', 'Guaranteed delivery service for FedEx Freight Priority or FedEx Freight Economy', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Priority', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_PRIORITY', 'false', 'Return FedEx Freight Priority rate quotes?', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Economy', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ECONOMY', 'false', 'Return FedEx Freight Economy rate quotes?', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Freight Class', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_FREIGHTCLASS', '050', 'Select a Freight Class', '6', '3', 'zen_cfg_select_option(array(\'050\', \'055\', \'060\', \'065\', \'070\', \'077\', \'085\', \'092\', \'100\', \'110\', \'125\', \'150\', \'175\', \'200\',  \'250\', \'300\',  \'400\', \'500\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Weight Units', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_WEIGHT', 'LB', 'Weight Units:', '6', '10', 'zen_cfg_select_option(array(\'LB\', \'KG\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Shipping unit maximum weight','MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_MAX_WEIGHT','19999','Maximum weight that you will ship per shipping unit','6','11', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Shipping unit name','MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIPPING_UNIT_NAME','Pallet(s)','Name of the shipping unit to display (Pallet, Skid...)','6','12', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Fixed surcharge', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SURCHARGE', '0', 'Fixed surcharge amount to add to shipping charge?', '6', '4', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Variable surcharge', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SURCHARGE_PERCENT', '0', 'Percentage surcharge amount to add to shipping charge?', '6', '4', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Tax Class', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_TAX_CLASS', '0', 'Use the following tax class on the shipping fee.', '6', '5', 'zen_get_tax_class_title', 'zen_cfg_pull_down_tax_classes(', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Shipping Zone', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ZONE', '0', 'If a zone is selected, only enable this shipping method for that zone.', '6', '98', 'zen_get_zone_class_title', 'zen_cfg_pull_down_zone_classes(', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('<b>3rd Party Bill To:</b>', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO', 'false', 'Are you Using 3rd Party Bill to? (Are you Dropshipping?)', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Your FedEx 3rd Party Account Number', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_3RD_PARTY_ACT_NUM', '', 'Enter the FedEx Freight 3rd Party Account Number assigned to you', '6', '2', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Bill To Address', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_ADDRS', '', 'Address for 3rd Party Bill To', '6', '', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Bill To City', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_CITY', '', 'City for 3rd Party Bill To', '6', '', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Bill To State', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_STATE', '', 'State for 3rd Party Bill To', '6', '', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Bill To Zip', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_ZIP', '', 'Zip for 3rd Party Bill To', '6', '', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Bill To Country', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_CTRY', '', 'Country for 3rd Party Bill To', '6', '', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ship From Address', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_ADDRS', '', 'Address for Ship From', '6', '', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ship From City', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_CITY', '', 'City for Ship From', '6', '', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ship From State', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_STATE', '', 'State for Ship From', '6', '', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ship From Zip', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_ZIP', '', 'Zip for Ship From', '6', '', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ship From Country', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_CTRY', '', 'Country for Ship From', '6', '', now())");
/*		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Liftgate', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_LIFTGATE', 'false', 'Calculate with Liftgate', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function,date_added) values ('Freezable', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_FREEZABLE', 'false', 'Calculate as Freezable', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Hazardous Material', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_HAZMAT', 'false', 'Calculate as Hazardous Materials', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");*/
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Ready to Ship', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_READY_TO_SHIP', 'false', 'Enable using products_ready_to_ship_freight field (requires Numinix Product Fields optional dimensions fields) to identify products which ship separately?', '6', '10', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Debug', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_DEBUG', 'false', 'Turn On Debugging?', '6', '3', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Shipping Info', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_INFO', '', 'Add a description that will display in Fast and Easy AJAX Checkout', '6', '99', 'zen_cfg_textarea(', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SORT_ORDER', '0', 'Sort order of display.', '6', '99', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Enable Address Validation','MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ADDRESS_VALIDATION','false','Would you like to use the FedEx Address Validation service to determine if an address is residential or commercial?','6','9','zen_cfg_select_option(array(\'true\',\'false\'),',now())");
	}

	function remove() {
		global $db;

		$db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
	}

	function keys() {
		return array(
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_STATUS',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_VERSION',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ACT_NUM',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_METER_NUM',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_REGKEY',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIPPER_ACT_NUM',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_CONNECTION_TIMEOUT',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_GUARANTEE',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_PRIORITY',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ECONOMY',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_FREIGHTCLASS',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_WEIGHT',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SURCHARGE',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SURCHARGE_PERCENT',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_TAX_CLASS',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ZONE',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SORT_ORDER',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_3RD_PARTY_ACT_NUM',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_ADDRS',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_CITY',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_ZIP',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_STATE',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_BILL_TO_CTRY',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_ADDRS',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_CITY',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_ZIP',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_STATE',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIP_FROM_CTRY',
/*			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_LIFTGATE',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_FREEZABLE',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_HAZMAT',*/
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_ADDRESS_VALIDATION',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_READY_TO_SHIP',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_INFO',
			'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_DEBUG',
		    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_MAX_WEIGHT',
		    'MODULE_SHIPPING_FEDEX_WEB_SERVICES_FREIGHT_SHIPPING_UNIT_NAME'
		);
	}
}
