Here are the instructions to the Freight Rating Sample Code.
 
1.       FedEx has a Developer Resource Center (DRC) located at www.fedex.com/developer.  You can register on that site and create a user id and password to have access to the Technical Resources available. 

2.       Click the �FedEx Web Services� link in the list running along the left side of the landing page.

3.       You will end up on the �FedEx Web Services� landing page.  Click on the �Move To Documentation� link under Step 1 �Documentation and Downloads� section about halfway down the page.

4.       Scroll to the bottom of the page to find web services split across two tabs.  The Standard tab identifies web services that do not require special permissions to be implemented.  The Advanced tab contains web service that should be developed in test and then moved into production after debugging is complete.  Services that create a shipping label require label certification before they can be used in production.

5.       This landing page contains documentation, Web Service WSDLs (the �WSDL� link in each gray header), Schemas (the �XML� link in each gray header), and sample code in Java, PHP, C# and VB.Net for each of the Web Services FedEx offers. 

a.       Rate Available Services listed under the �Standard Services� tab at the bottom of the page.  This area offers the Rate WSDL and the Rate Schema, documentation and the sample code. 

                                                               i.      Download the PHP Sample Code under �Rate� or �Rate Available Services�.  After unzipping the examples, in the Rate Client folder you will find three folders.  One is for Freight rating.

 

I have attached an example XML request for a basic Freight Third Party Rate request and an example setting up a liftgate delivery special service.  Here are some bullet points I send to people about Freight rating using the web service.

 

�         LTL Rating The Rate Web Service can be set up to get a rate back for a specific LTL Shipping service or to return a list of LTL services available to the recipient's zip code with an estimated rate for each service (also known as Rate Available Services).  Any Rate requests can include optional information detailed below and more.  These parameters are included in the request and the rates returned will reflect any effects these parameters have.  All replies reflect your discounted rates by default.  You can add a parameter in the request to get published rates (also called List Rates; they have no discounts) back too.  The more information included in the rate request, the more accurate the rate quote(s) will be.
o    Information:
�  There is minimum information needed to generate rates.  Optional data can also be added to make the rate estimates more accurate.  Basic data that will produce transportation costs for Domestic and International shipping is:
�  Package weight
�  Shipping location postal code and country
�  Destination postal code and country
�  Valid destinations are in the continental US, Canada and Mexico.
�  Shipments to other countries send the PostalCode value, but have different country codes.  I suggest you download the full developer guide.
�  Log in to the FedEx Developer Resource Center (DRC) at https://www.fedex.com/us/developer/web-services/process.html?tab=tab1.  You will need to log in with your DRC User ID and Password.

�  After logging in you be on the "FedEx Web Services" landing page and in the �Documentation and Downloads� tab.  Click the �Download the FedEx Web Services Developer Guide� link just above the purple graphic of a page.  The PDF is about 8MB.

�  Click the �Download the FedEx Web Services Developer Guide� link displayed at the top of the webpage.
�  Country Codes are listed in Appendix A.
�  Appendix J lists all the countries that use a Postal Code system and show the formats of their codes.
�  Countries without Postal Codes use City and Country code
�  Appendices B, C, D & E list the US, Canada, Mexico and United Arab Emirates State or Province Codes.
�  Appendix P lists the countries that accept Electronic Trade Documents (ETD).  If a country doesn't accept ETD then paper documents must be sent, in pouches attached to the packaging.
�  Estimated shipping date
�  Authentication information
�  Key
�  Password
�  FedEx Express small package Account number
�  Assigned FedEx Meter number
�  Additional Optional information can include:
�  Special Services
�  Package dimensions
�  Residential Destination
�  Time In Transit flag
�  List Rate flag
�  Non Standard Shipping Container
�  When using Rate to return a list of Available Services (Rate Available Services), only valid services will be returned.  If one of the shipping services is not available then it will not be included on the list.
o    Please note these coding tips:
�  When testing, the test environment does not return accurate rates in the reply.  The environment is for working out the bugs and shouldn't be used to check rates.  The Production environment does return the discounts and will give correct rates because it uses the same database the billing servers use. Please use the Production environment for testing when possible.
�  When sending the date, please follow the DateTime example.  The format is YYYY-MM-DDTHH:MM:SS-05:00  The "T" identifies the start of the Time section and the -05:00 in my example identifies the Eastern Time Zone offset from Greenwich Mean Time (aka Zulu time).  Please adjust the offset for your time zone. 
�  The complete current  list of services that might be returned in a reply is:
�  LTL Freight

�  FEDEX_FREIGHT_ECONOMY

�  FEDEX_FREIGHT_PRIORITY

�  When sending the estimated ship date, if your shipping department only ships on Monday through Friday, please only send Monday through Friday dates. 

�  The Rate Available Services Web Service allows the option of sending package dimensions for large, bulky packages.  Any affect the package size has on the rate will be reflected in the rates you get back.
�  Transit Time is available but not returned by default. 
�  Set the <ReturnTransitAndCommit> to �true� to get Transit Time in the request.
�  FedEx Freight can return a Time in Transit.
�  For LTL, a Date, day of the week and a Transit Time in days is in the reply for each service.
�  One rate request can be for a single handling unit or more than one
�  Hazardous Materials/Dangerous Goods are two different systems to define handling hazardous chemicals inside the package.
�  Add DANGEROUS_GOODS as a Special ServiceType for its charge to be included in the Rates.
�  FedEx Freight estimates are available too, using additional parameters in the request.
�  If you are using the Test environment, your Developer Test Key will also include dummy LTL Freight account information that will work only in test.  You will need a FedEx Freight account and its Billing Address to rate LTL in Production .
�  You can include a ServiceType in the request of either FEDEX_FREIGHT_ECONOMY or FEDEX_FREIGHT_PRIORITY to get an estimate for that specific service, or don�t send a Service Type at all and FedEx will return a list of available services with an estimate for each service.
�  Transit Time is available but not returned by default.   Set the ReturnTransitAndCommit to �true� to get Transit Time in the reply.
�  For LTL, a Date, day of the week and a Transit Time in days are in the reply for each service.
�  LTL Freight service estimates are only available when the rate request includes a Freight Shipment Detail section that specifies the LTL details needed to generate a rate.
�  .
�  As in the examples, when the PaymentType is PREPAID, the Role is SHIPPER and you use the Alternate Billing section for the Freight account number and address, Web Services will rate from any zip code to any zip code.
�  The Freight account number must be set up in FedEx�s backend as a third party account before this will work in the transactions.
�  As in the examples, when the PaymentType is PREPAID and the Role is SHIPPER, Web Services expects your shipping address to match the billing address of your Freight account number.  You will get an error if it is different.
�  As in the examples, when the PaymentType is COLLECT and the Role is CONSIGNEE, Web Services expects your Destination address to match the billing address of your Freight account number.  You will get an error if it is different.
�  FedEx Freight does not allow the addition of Variable Handling Charges in the Rate request.
�  An individual Service Type can be specified or the Service Type doesn�t need to be included and you will receive back a list of available services, with rates for each service.
�  Each LTL service returned includes a <QuoteNumber> with the rates.  Keep the Quote Number for the chosen service.  A FedEx Sales Account Executive will need that number to resolve any future billing issues.
�  A Freight Account Number and Billing address must be specified in the Freight Shipment Detail.
�  Account numbers and billing addresses are validated when a transaction is run.
�  For LTL Freight, the TotalNetCharge value under a Rate Type of PAYOR_ACCOUNT_SHIPMENT will provide the final discounted (Account) rate charges for each of the various services.
�  For LTL Freight, the TotalNetCharge value under a Rate Type of PAYOR_LIST_SHIPMENT will provide the final List rate charges for each of the various services.
�  A guide how to fill out a FedEx Bill of Lading that might provide some insight at http://images.fedex.com/us/freight/pdf/fxfbol_instructions.pdf?link=4
�  LTL Accessorial Charges (Special Services and Fees) are explained at http://images.fedex.com/us/freight/rulestariff/AccessorialRates.pdf
�  From the WSDL/Schema, I think the list of LTL Special Services is:
�  CALL_BEFORE_DELIVERY
�  COD
�  CUSTOM_DELIVERY_WINDOW
�  DANGEROUS_GOODS
�  DO_NOT_BREAK_DOWN_PALLETS
�  DO_NOT_STACK_PALLETS
�  EXTREME_LENGTH
�  FREIGHT_GUARANTEE
�  HOLD_AT_LOCATION
�  HOME_DELIVERY_PREMIUM (?)
�  INSIDE_DELIVERY
�  INSIDE_PICKUP
�  LIFTGATE_DELIVERY
�  LIFTGATE_PICKUP
�  LIMITED_ACCESS_DELIVERY
�  LIMITED_ACCESS_PICKUP
�  PROTECTION_FROM_FREEZING
�  SATURDAY_DELIVERY (?)
�  SATURDAY_PICKUP (?)
�  TOP_LOAD
�  Please note LTL Freight cannot be auto-quoted to/from the US and Puerto Rico, Alaska or Hawaii or to any country that has an ocean component to the shipment.
�  International Freight can include Duties and Taxes and Fees, but commodity information and the Harmonized Code for the commodity must be listed in the request.
 